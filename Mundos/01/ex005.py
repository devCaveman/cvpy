"""
    Crie um programa que peça um número ao usuário, e mostre seu antecessor e sucessor.
"""
# pedindo um número ao usuário.
num = int(input('Digite um número: '))

# calculando antecessor e sucessor.
ant = num - 1
suc = num + 1

# mostrando o resultado.
print('O antecessor de {0} é {1}, e seu sucessor é {2}'.format(num, ant, suc))

# mostrando o resultado usando apenas uma variável.
print('O antecessor de {0} é {1}, e seu sucessor é {2}'.format(num, (num - 1), (num + 1)))

