"""
    Crie um programa que leia duas notas e mostre o a média.
"""
# Lendo as notas.
nota1 = float(input('Digite o valor da primeira nota: '))
nota2 = float(input('Digite o valor da segunda nota: '))

# Calculando.
media = ( nota1 + nota2 ) / 2
# Mostrando o resultado.
print('A média entre {:.1f} e {:.1f} é {:.1f}'.format(nota1, nota2, media))

