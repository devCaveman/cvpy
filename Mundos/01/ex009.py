"""
    Crie um script que irá ler um número qualquer, mostre a 
tabuáda desse número.

"""

# Lendo um número.
num = int(input('Digite um número e veja sua tabuáda: '))

# Função que desenha linhas e colunas
def desenhar_linhas(tamanho : int):
    print('*', '-'*(tamanho - 2), '*')  

# Exibindo a tábuada
desenhar_linhas(11)
for multiplo in range(1, 11):
    print('¦ {0} x {1} = {2} ¦'.format(num, multiplo, (num * multiplo)))

desenhar_linhas(11)

