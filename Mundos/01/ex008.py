"""
    Crie um algoritimo que leia uma medida em metros e de seu
valor correspondente em quilômetros, hectômetros, decâmetros,
decímetros, centímetros e milímetros.

"""

# Lendo um valor de medida
metros = float(input('Digite uma distância em métros: '))

# Calculando lista de medidas
lista_medidas = [
        '{:.0f}dm'.format(metros * 10),
        '{:.0f}cm'.format(metros * 100),
        '{:.0f}mm'.format(metros * 1000),
        '{}dam'.format(metros / 10),
        '{}hm'.format(metros / 100),
        '{}km'.format(metros / 1000)
        ]

# Mostrando o resultado
print('A medida de {}m corresponde à: '.format(metros))
for i in range(len(lista_medidas)):
    print(lista_medidas[i])
