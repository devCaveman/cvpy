"""
Faça um programa que leia algo pelo teclado e mostre na tela o seu
tipo primitivo e todas as imformações possiveis sobre ele.
"""
# Capiturando uma entrada pelo teclado
algo = input('Digite qualquer coisa: ')

# Identificando qual o tipo e tudo mais...
tipos_e_mais = {
        'tipo primitivo' : type(algo),
        'somente espaços' : algo.isspace(),
        'é um número' : algo.isnumeric(),
        'é alfabético' : algo.isalpha(),
        'é alfanumérico' : algo.isalnum(),
        'está em maiusculas' : algo.isupper(),
        'está em minusculas' : algo.islower(),
        'está captalizado' : algo.istitle(),
        'está em ascii' : algo.isascii()
}

# Imprimindo...
for key, value in tipos_e_mais.items():
    print('{0} {1} = {2}'.format(algo, key, value))

