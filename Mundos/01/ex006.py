"""
    Crie um algorítimo que leia um número, mostre o seu dobro, triplo e raiz quadrada.
"""
# Lendo um número.
num = int(input('Digite um número: '))

# Calculando.
calc_dict = {
        'O dobro': num * 2,
        'O triplo': num * 3,
        'A raiz quadrada': '{:.2f}'.format( num ** ( 1/2 ))
        }

# Mostrando o resultado.
for key, value in calc_dict.items():
    print('{0} de {1} é {2}'.format(key, num, value))

