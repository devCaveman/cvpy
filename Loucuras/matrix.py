#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
    Exemplo de uma matriz em python.
    Este código maluco e sem sentido, monta uma frase randomicamente com os elementos contidos na matriz.
"""
from random import randint as ri

# Plano cartesiano X e Y
matrix = [
        ['Thiago', 'Pedro', 'João', 'Sandro', 'Rafael', 'Guanabara'],
        ['é mestre em AI', 'é um jovem gafanhoto', 'é desenvolvedor python', 'é analista de sistemas', 'é sysadmin', 'é expecialista em devops', 'é cientista de dados'],
        ['com experiêcia de', 'que já estudou por incríveis', 'e estudou por'],
        [2, 10, 18, 23, 30, 12, 34, 60, 500, 3, 666],
        ['', 'fucking', '', '', '', '', '', 'mil', 'elevado à 7'],
        ['eras.', 'milênios.', 'anos.', 'horas.', 'luas.', 'meses.', 'minutos.', 'segundos.', 'milissegundos.']
]

# Inicia uma lista vazia
lista_de_frases = []

# Sorteia elementos da matrix e os adiciona na lista de frases
for y in range(len(matrix)):
    max_x_len = int(len(matrix[y])) - 1 #Começa a contar do 0, então o final é -1
    x_random_pos = ri(0, max_x_len)
    lista_de_frases.append(matrix[y][x_random_pos]) 

# Monta uma frase com os elementos sorteados, contidos na lista.
frase_completa = ' '.join(str(frase) for frase in lista_de_frases) 

print(frase_completa, '\n')

